import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class GameMethods {

    private Scanner scanner = new Scanner(System.in);
    private List<String> guessList = new ArrayList<>();
    private List<String> codeList = new ArrayList<>();


    void printMenu() {
        System.out.println("<<<<<<<<<<<<<>>>>>>>>>>>>");
        System.out.println("-------MASTERMIND-------");
        System.out.println("1. NEW GAME");
        System.out.println("2. EXIT");
        System.out.println("<<<<<<<<<<<<>>>>>>>>>>>");
    }

    void printNewGame() {
        System.out.println("ENTER A 4 NUMBER CODE, USING NUMBERS ASSIGNED TO COLORS:");
        System.out.println("1.BLACK");
        System.out.println("2.WHITE");
        System.out.println("3.RED");
        System.out.println("4.BLUE");
        System.out.println("5.YELLOW");
        System.out.println("6.BROWN");
        System.out.println("7.GREEN");
        System.out.println("8.PINK");
    }

    void menuSelect() {
        int menuSelect = scanner.nextInt();
        switch (menuSelect) {
            case 1:
                System.out.println("PLEASE ENTER FOUR NUMBERS, ASSIGNED TO COLORS, TO CREATE A COLOR CODE");
                printNewGame();
                String buffer = scanner.next();
                createCode(buffer);
                break;
            case 2:
                System.out.println("BYE!");
                break;
        }
    }

    List<String> createCode(String code) {
        char[] codeArray = code.toCharArray();
        for (int i = 0; i < 4; i++) {
            if (codeArray[i] == '1') {
                codeList.add("BLACK");
            } else if (codeArray[i] == '2') {
                codeList.add("WHITE");
            } else if (codeArray[i] == '3') {
                codeList.add("RED");
            } else if (codeArray[i] == '4') {
                codeList.add("BLUE");
            } else if (codeArray[i] == '5') {
                codeList.add("YELLOW");
            } else if (codeArray[i] == '6') {
                codeList.add("BROWN");
            } else if (codeArray[i] == '7') {
                codeList.add("GREEN");
            } else if (codeArray[i] == '8') {
                codeList.add("PINK");
            }
        }
        return codeList;
    }

    List<String> createGuessList() {
        System.out.println("PLEASE TYPE 4 COLORS FROM THE LIST AND PRESS ENTER:");
        System.out.println("BLACK, WHITE, RED, BLUE, YELLOW, BROWN, GREEN, PINK");
        for (int i = 0; i < 4; i++) {
            guessList.add(scanner.next().toUpperCase());
        }
        return guessList;
    }

    String guessCodeEasy() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 4; i++) {
            if (codeList.contains(guessList.get(i))) {
                if (codeList.get(i).equals(guessList.get(i))) {
                    builder.append("X");
                } else {
                    builder.append("O");
                }
            } else {
                builder.append("-");
            }
        }
        return builder.toString();
    }

    String guessCodeHard() {
        int guessPosition = 0;
        int guessContains = 0;
        for (int i = 0; i < 4; i++) {
            if (codeList.contains(guessList.get(i))) {
                if (codeList.get(i).equals(guessList.get(i))) {
                    guessPosition++;
                } else {
                    guessContains++;
                }
            }
        }
        return "RED PEGS: " + guessPosition + ", WHITE PEGS: " + guessContains;
    }

    void playTheGame() {
        if (scanner.next().toUpperCase().equals("EASY")) {
            while (true) {
                guessList = createGuessList();
                System.out.println(guessCodeEasy());
                int counter = 0;

                for (int i = 0; i < 4; i++) {
                    if (guessList.get(i).equals(codeList.get(i))) {
                        counter++;
                    }
                }
                guessList.clear();
                if (counter == 4) {
                    break;
                }
            }
            System.out.println("CONGRATULATIONS, YOU GUESSED THE CODE!");
        } else if(scanner.next().toUpperCase().equals("EASY")) {
            while (true) {
                guessList = createGuessList();
                System.out.println(guessCodeHard());
                int counter = 0;
                for (int i = 0; i < 4; i++) {
                    if (guessList.get(i).equals(codeList.get(i))) {
                        counter++;
                    }
                }
                guessList.clear();
                if (counter == 4) {
                    break;
                }
            }
            System.out.println("CONGRATULATIONS, YOU GUESSED THE CODE!");
        } else {
            System.out.println("PLEASE TYPE IN \"EASY\" OR \"HARD\"");
            playTheGame();
        }

    }
}



